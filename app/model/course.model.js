// import thư viện mongoose
const mongoose = require("mongoose");

// khai báo class schema của thư viện
const schema = mongoose.Schema; //giống như khai báo class trong ES6

// Khai báo course schema
const courseSchema = new schema ({
    //Trường Id
    _id: {
        type: mongoose.Types.ObjectId,
        require: true
    },
    courseCode:{
        type: String,
        unique: true,
        required: true
    },
    courseName:{
        type: String,
        required: true
    },
    price:{
        type: Number,
        required: true
    },
    discountPrice:{
        type: Number,
        required: false
    },
    duration:{
        type: String,
        required: true
    },
    level:{
        type: String,
        required: true
    },
    coverImage:{
        type: String,
        required: true
    },
    teacherName:{
        type: String,
        required: true
    },
    teacherPhoto:{
        type: String,
        required: true
    },
    isPopular:{
        type: Boolean,
        default: true
    },
    isTrending: {
        type: Boolean,
        default: true
    },
    // trường ngày tạo
    ngayTao:{
        type: Date,
        default: Date.now()
    },
    // trường ngày cập nhật
    ngayCapNhat:{
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("Course",courseSchema);
const express = require("express"); // Tương tự : import express from "express";
const path = require("path");
//khai báo router
const {CourseRouter} = require("./app/router/course.router")


// Khởi tạo Express App
const app = express();

const port = 8000;

// import thư viện mogoose
const mongoose = require("mongoose");

//sử dụng được body json
app.use(express.json());

//sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

mongoose.set('strictQuery', true);

//middleware static
app.use(express.static(__dirname + '/views'));

// nơi khai báo API
app.get("/index",(req,res)=>{
    res.sendFile(path.join(__dirname + "/views/index.html"));
})

app.get("/CRUD",(req,res)=>{
    res.sendFile(path.join(__dirname + "/views/ListCourses.html"));
})

// kết nối với mongodb
mongoose.connect(`mongodb://127.0.0.1:27017/CRUD_Course365`, function(error) {
    if (error) throw error;
    console.log('Connect to MongooseDB Successfully !');
   })


   // Sử dụng router
   app.use("/",CourseRouter);
   
app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
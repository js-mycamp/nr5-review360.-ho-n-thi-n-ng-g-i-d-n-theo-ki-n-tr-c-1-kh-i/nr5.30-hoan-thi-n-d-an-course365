var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
}

var gAllCourses =[];
var gCoursesPopular = [];
var gCoursesTrending = [];
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    
  
   
})
onPageLoading();

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading(){
    $.ajax({
        url: "https://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses",
        type: 'GET',
        dataType: 'json', // added data type
        success: function (responseObject) {
          gAllCourses = responseObject;
            console.log(responseObject);
            // hàm lấy obj courses Popular
            coursesPopular();
             // hàm lấy obj courses Trending
   coursesTrending();
        },
        error: function (ajaxContext) {
          alert(ajaxContext.responseText)
        }
      });
   
    
}
//  hàm gọi API
function callApicourses() {
    $.ajax({
      url: "https://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses",
      type: 'GET',
      dataType: 'json', // added data type
      success: function (responseObject) {
        gAllCourses = responseObject;
          console.log(responseObject);
         
        //    console.log(JSON.stringify(responseObject));  //response text
        //4 xử lý hiện thị
       // loadDataToTable(responseObject)  // responseObject là array 
      },
      error: function (ajaxContext) {
        alert(ajaxContext.responseText)
      }
    });
  }

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm xử lý lọc lấy obj courses Popular
function coursesPopular() {
    var vGioiHanSoTrangDuocThemVao = 0;
    for (var i = 0; i < gAllCourses.length; i++) {
        if (gAllCourses[i].isPopular === true && vGioiHanSoTrangDuocThemVao < 4) {
            vGioiHanSoTrangDuocThemVao++
            gCoursesPopular = (gAllCourses[i])
            let Card = `
            <div class="col-sm-3 border-light mb-3">
            <div class="card">
                <div class="card-header">
                    <img class="card-img-top rounded-top" src="${gCoursesPopular.coverImage}" alt="">
                </div>
                <div class="card-body">
                    <h6 class="card-title text-primary">${gCoursesPopular.courseName}</h6>
                    <p class="card-text"><i class="fa-regular fa-clock"></i> ${gCoursesPopular.duration} &nbsp; ${gCoursesPopular.level}</p>
        
                    <p class="card-text">
                        <span class="font-weight-bold">${gCoursesPopular.price}</span> <span class="text-secondary"><del>${gCoursesPopular.discountPrice} $550</del></span>
                    </p>
                </div>
                <div class="card-footer form-inline justify-content-between">
                    <img class="rounded-circle w-25" style="max-width:20%;" src=${gCoursesPopular.teacherPhoto} alt="">
                        <p class="card-text form-inline w-75 justify-content-between"><small> ${gCoursesPopular.teacherName}</small> <i
                            class="fa-regular fa-bookmark"></i></p>
                </div>
            </div>
        </div>
             `

            document.querySelector(".Popular").innerHTML += Card;
        }
    }
}
// hàm xử lý lọc lấy obj courses Treding
function coursesTrending() {
    var vGioiHanSoTrangDuocThemVao = 0;
    for (var i = 0; i < gAllCourses.length; i++) {
        if (gAllCourses[i].isTrending === true && vGioiHanSoTrangDuocThemVao < 4) {
            vGioiHanSoTrangDuocThemVao++
            gCoursesPopular = (gAllCourses[i])
            let Card = `
            <div class="col-sm-3 border-light mb-3">
            <div class="card">
                <div class="card-header">
                    <img class="card-img-top rounded-top" src="${gCoursesPopular.coverImage}" alt="">
                </div>
                <div class="card-body">
                    <h6 class="card-title text-primary">${gCoursesPopular.courseName}</h6>
                    <p class="card-text"><i class="fa-regular fa-clock"></i> ${gCoursesPopular.duration} &nbsp; ${gCoursesPopular.level}</p>
        
                    <p class="card-text">
                        <span class="font-weight-bold">${gCoursesPopular.price}</span> <span class="text-secondary"><del>${gCoursesPopular.discountPrice} $550</del></span>
                    </p>
                </div>
                <div class="card-footer form-inline justify-content-between">
                    <img class="rounded-circle w-25" style="max-width:20%;" src=${gCoursesPopular.teacherPhoto} alt="">
                        <p class="card-text form-inline w-75 justify-content-between"><small> ${gCoursesPopular.teacherName}</small> <i
                            class="fa-regular fa-bookmark"></i></p>
                </div>
            </div>
        </div>
             `

            document.querySelector(".Trending").innerHTML += Card;
        }
    }
}



